import  Joi from 'joi';

//github id (since) validation
export function validateId(req) {
    const schema = {
        since: Joi.number()
    };
    return Joi.validate(req,schema);
}

//github username validation
export function validateUsername  (req) {
    const schema = {
        username: Joi.string().min(2).max(39).regex(/^[a-zA-Z0-9]*$/)
    };
    return Joi.validate(req,schema);
};
