
import express from 'express';

import {usersList,userDetails, userRepos} from '../controllers/users';

export const router = express.Router();

router.get('/', usersList);

router.get('/:username/details', userDetails);

router.get('/:username/repos', userRepos);
