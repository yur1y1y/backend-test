import config from 'config';

module.exports = function () {
    if (!config.get('githubToken')) {
        throw new Error('FATAL ERROR: github token is not defined.');
    }

    if (!config.get('usersApi')) {
        throw new Error('FATAL ERROR: users api is not defined.');
    }
};
