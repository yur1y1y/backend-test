
import helmet from 'helmet';

module.exports = function (app) {
    app.use(helmet());
};
