import error from '../middleware/error';
import {router as users} from '../routes/users';

module.exports = (express,app) => {
    app.use(express.json());
    app.use('/api/users', users);
    app.use(error);
};
