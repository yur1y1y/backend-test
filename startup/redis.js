
 const redis = require('redis');

import RateLimit from 'express-rate-limit';
import RedisStore from 'rate-limit-redis';

export let client;
let limiter;
 // create and connect redis client to local instance.
if(process.env.REDISCLOUD_URL ) {
    const url = require('url');

    const redisURL = url.parse(process.env.REDISCLOUD_URL);

    client= redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});

    client.auth(redisURL.auth.split(":")[1]);
}

else {
    client = redis.createClient(6379);
}

module.exports = function(app) {

// echo redis errors to the console
    client.on('error', (err) => {
        console.log("Error " + err);
    });

    limiter = new RateLimit({
        store: new RedisStore({
            client:client
        }),
        max: 100, // limit each IP to 100 requests per windowMs
        delayMs: 0 // disable delaying - full speed until the max limit is reached
    });

 // apply to all requests
    app.use(limiter);
};
