// start script

import winston from 'winston';
import express from 'express';
import cluster from 'cluster';

if (cluster.isMaster) {
    // Create a worker for each CPU
    for (let i = 0; i < require('os').cpus().length; i++) {
        cluster.fork();
    }

    cluster.on('online', function (worker) {
        console.log('Worker ' + worker.process.pid + ' is online.');
    });
    cluster.on('exit', function (worker, code, signal) {
        console.log('worker ' + worker.process.pid + ' died.');
    });
} else {

    const app = express();

    require('./startup/logging')();
    require('./startup/cors')(app);
    require('./startup/helmet')(app);
    require('./startup/redis')(app);
    require('./startup/routes')(express, app);
    require('./startup/config')();

    const port = process.env.PORT || 3000;

    if (!module.parent) module.exports = app.listen(port, () => winston.info(`Listening on port ${port}...`));
}
