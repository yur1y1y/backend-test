import {validateId, validateUsername} from '../models/users';
import fetch from 'node-fetch';
import config from 'config';
import {client as redis } from '../startup/redis';
const usersApi = config.get('usersApi');

//Authenticated requests get a higher rate limit
const options = {
    headers: {
        Authorization: config.get('githubToken')
    }
};

export  const usersList = async(req, res) => {
    const {query}= req;

    const {error} = validateId({since:Number(query.since)});
    if (error) return res.status(400).send(error.details[0].message);

    // key to store results in Redis store
    const usersRedisKey = `user:since:${req.query.since}`;
    return redis.get(usersRedisKey, async (err, result) => {

        if (result) {
            return res.send(JSON.parse(result));
        } else {

            const getNextLink = links => links.slice(links.indexOf('<') + 1, links.indexOf(`>`));

            const result = await fetch(`${usersApi}?since=${query.since}`, options)
                .then(async res => {
                    return {users: await res.json(), next: getNextLink(res.headers.get('Link'))};
                });

            // Save the  API response in Redis store,  data expire time in 3600 seconds, it means one hour
            redis.setex(usersRedisKey, 3600, JSON.stringify(result));

          return res.send(result);
        }
    });
};

export const userDetails = async(req, res) => {

    const {username} =req.params;

    const {error} = validateUsername(req.params);
    if (error) return res.status(400).send(error.details[0].message);

    // key to store results in Redis store
    const usersDetailsKey = `user:details:${username}`;

    return redis.get(usersDetailsKey, async (err, result) => {

        if (result) {
            return res.send(JSON.parse(result));
        } else {

            const result =await fetch(`${usersApi}/${username}`,options)
                .then(async res => {
                    return {details: await res.json() };
                });

            // Save the  API response in Redis store,  data expire time in 3600 seconds, it means one hour
            redis.setex(usersDetailsKey, 3600, JSON.stringify(result));
           return res.send(result);
        }
    });
};

export const userRepos = async(req, res) => {

    const {username} =req.params;

    const {error} = validateUsername(req.params);
    if (error) return res.status(400).send(error.details[0].message);

    const userReposKey = `user:repos:${username}`;

    return redis.get(userReposKey, async (err, result) => {

        if (result) {
            return res.send(JSON.parse(result));
        } else {

            const result =await fetch(`${usersApi}/${username}/repos`,options)
                .then(async res => {

                    return {repos: await res.json() };
                });

            // Save the  API response in Redis store,  data expire time in 3600 seconds, it means one hour
            redis.setex(userReposKey, 3600, JSON.stringify(result));
            return res.send(result);
        }
    });
};
